package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class EndScreenWithRecord implements Initializable {

    @FXML
    Button Start;

    @FXML
    Button records;

    @FXML
    TextField txt;

    @FXML
    public void set(){

        Integer num = 0;

        String fileName = "src/sample/output.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                Integer number = Integer.parseInt(line);
                num = number;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        txt.setText(""+num);
    }

    @FXML
    public void StartButton(ActionEvent event) throws IOException {
        Parent game = FXMLLoader.load(getClass().getResource("Game.fxml")); //betolt
        Scene gameScene = new Scene(game, 400, 550);
        Stage gameStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        gameStage.setScene(gameScene);
        gameStage.show();
    }

    @FXML
    public void RekordButton(ActionEvent event)throws IOException {
        Parent rekord = FXMLLoader.load(getClass().getResource("Rekord.fxml")); //betolt
        Scene gameScene = new Scene(rekord,400,550);
        Stage gameStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        gameStage.setScene(gameScene);
        gameStage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        set();
    }
}
