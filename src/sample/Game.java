package sample;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Arc;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Game {

    @FXML
    Button end;

    @FXML
    ImageView cardBack1;

    @FXML
    ImageView cardBack2;

    @FXML
    ImageView cardBack3;

    @FXML
    ImageView cardBack4;

    @FXML
    ImageView cardBack5;

    @FXML
    ImageView cardBack6;

    @FXML
    ImageView cardBack7;

    @FXML
    ImageView cardBack8;

    @FXML
    ImageView cardBack9;

    @FXML
    ImageView cardBack10;

    @FXML
    ImageView cardBack11;

    @FXML
    ImageView cardBack12;

    @FXML
    ImageView card1;

    @FXML
    ImageView card2;

    @FXML
    ImageView card3;

    @FXML
    ImageView card4;

    @FXML
    ImageView card5;

    @FXML
    ImageView card6;

    @FXML
    ImageView card7;

    @FXML
    ImageView card8;

    @FXML
    ImageView card9;

    @FXML
    ImageView card10;

    @FXML
    ImageView card11;

    @FXML
    ImageView card12;

    @FXML
    Arc timer;

    Boolean done = false;

    ArrayList<Pair<Integer,Integer>> positions = new ArrayList<Pair<Integer,Integer>>();

    HashMap<Integer, Integer> whereIsTheCard = new HashMap<Integer, Integer>();

    Boolean wasActionOnCard = false;

    Integer selectedCard = 0;

    Integer selectedCard2 = 0;

    Integer coutor = 0;

    Integer endtime = 1;

    Boolean newRecord = false;

    private void genereateCardPositions(){


        Thread runTimer = new runTheTimer();
        runTimer.start();

        ArrayList<Pair <Double,Double>> layoutOfCardBacks = new ArrayList<Pair<Double, Double>>();
        Pair<Double,Double> ll = new Pair<Double,Double>(30.0,30.0);
        layoutOfCardBacks.add(ll); ll = new Pair<Double,Double>(30.0,120.0);
        layoutOfCardBacks.add(ll); ll = new Pair<Double,Double>(30.0,210.0);
        layoutOfCardBacks.add(ll); ll = new Pair<Double,Double>(30.0,300.0);
        layoutOfCardBacks.add(ll); ll = new Pair<Double,Double>(160.0,30.0);
        layoutOfCardBacks.add(ll); ll = new Pair<Double,Double>(160.0,120.0);
        layoutOfCardBacks.add(ll); ll = new Pair<Double,Double>(160.0,210.0);
        layoutOfCardBacks.add(ll); ll = new Pair<Double,Double>(160.0,300.0);
        layoutOfCardBacks.add(ll); ll = new Pair<Double,Double>(290.0,30.0);
        layoutOfCardBacks.add(ll); ll = new Pair<Double,Double>(290.0,120.0);
        layoutOfCardBacks.add(ll); ll = new Pair<Double,Double>(290.0,210.0);
        layoutOfCardBacks.add(ll); ll = new Pair<Double,Double>(290.0,300.0);
        layoutOfCardBacks.add(ll);
        ArrayList<Integer> listOfPositions = new ArrayList<Integer>();
        listOfPositions.add(1);
        listOfPositions.add(2);
        listOfPositions.add(3);
        listOfPositions.add(4);
        listOfPositions.add(5);
        listOfPositions.add(6);
        listOfPositions.add(7);
        listOfPositions.add(8);
        listOfPositions.add(9);
        listOfPositions.add(10);
        listOfPositions.add(11);
        listOfPositions.add(12);
        java.util.Collections.shuffle(listOfPositions);
        for (int i=0;i<6;i++) {
            Pair<Integer, Integer> tmp = new Pair<>(listOfPositions.get(i),listOfPositions.get(11-i));
            positions.add(tmp);
        }
        Pair tmp = layoutOfCardBacks.get(positions.get(0).getKey()-1);
        cardBack1.setLayoutX((double)tmp.getValue());
        cardBack1.setLayoutY((double)tmp.getKey());
        whereIsTheCard.put(positions.get(0).getKey(),1);

        tmp = layoutOfCardBacks.get(positions.get(1).getKey()-1);
        cardBack2.setLayoutX((double)tmp.getValue());
        cardBack2.setLayoutY((double)tmp.getKey());
        whereIsTheCard.put(positions.get(1).getKey(),2);

        tmp = layoutOfCardBacks.get(positions.get(2).getKey()-1);
        cardBack3.setLayoutX((double)tmp.getValue());
        cardBack3.setLayoutY((double)tmp.getKey());
        whereIsTheCard.put(positions.get(2).getKey(),3);


        tmp = layoutOfCardBacks.get(positions.get(3).getKey()-1);
        cardBack4.setLayoutX((double)tmp.getValue());
        cardBack4.setLayoutY((double)tmp.getKey());
        whereIsTheCard.put(positions.get(3).getKey(),4);


        tmp = layoutOfCardBacks.get(positions.get(4).getKey()-1);
        cardBack5.setLayoutX((double)tmp.getValue());
        cardBack5.setLayoutY((double)tmp.getKey());
        whereIsTheCard.put(positions.get(4).getKey(),5);


        tmp = layoutOfCardBacks.get(positions.get(5).getKey()-1);
        cardBack6.setLayoutX((double)tmp.getValue());
        cardBack6.setLayoutY((double)tmp.getKey());
        whereIsTheCard.put(positions.get(5).getKey(),6);

        tmp = layoutOfCardBacks.get(positions.get(0).getValue()-1);
        cardBack7.setLayoutX((double)tmp.getValue());
        cardBack7.setLayoutY((double)tmp.getKey());
        whereIsTheCard.put(positions.get(0).getValue(),7);


        tmp = layoutOfCardBacks.get(positions.get(1).getValue()-1);
        cardBack8.setLayoutX((double)tmp.getValue());
        cardBack8.setLayoutY((double)tmp.getKey());
        whereIsTheCard.put(positions.get(1).getValue(),8);


        tmp = layoutOfCardBacks.get(positions.get(2).getValue()-1);
        cardBack9.setLayoutX((double)tmp.getValue());
        cardBack9.setLayoutY((double)tmp.getKey());
        whereIsTheCard.put(positions.get(2).getValue(),9);


        tmp = layoutOfCardBacks.get(positions.get(3).getValue()-1);
        cardBack10.setLayoutX((double)tmp.getValue());
        cardBack10.setLayoutY((double)tmp.getKey());
        whereIsTheCard.put(positions.get(3).getValue(),10);


        tmp = layoutOfCardBacks.get(positions.get(4).getValue()-1);
        cardBack11.setLayoutX((double)tmp.getValue());
        cardBack11.setLayoutY((double)tmp.getKey());
        whereIsTheCard.put(positions.get(4).getValue(),11);


        tmp = layoutOfCardBacks.get(positions.get(5).getValue()-1);
        cardBack12.setLayoutX((double)tmp.getValue());
        cardBack12.setLayoutY((double)tmp.getKey());
        whereIsTheCard.put(positions.get(5).getValue(),12);
    }

    private void turnCard(Integer cardNumber){
        switch (whereIsTheCard.get(cardNumber)) {
            case 1 :
                cardBack1.setVisible(true);
                break;
            case 2:
                cardBack2.setVisible(true);
                break;
            case 3:
                cardBack3.setVisible(true);
                break;
            case 4 :
                cardBack4.setVisible(true);
                break;
            case 5 :
                cardBack5.setVisible(true);
                break;
            case 6:
                cardBack6.setVisible(true);
                break;
            case 7 :
                cardBack7.setVisible(true);
                break;
            case 8:
                cardBack8.setVisible(true);
                break;
            case 9:
                cardBack9.setVisible(true);
                break;
            case 10:
                cardBack10.setVisible(true);
                break;
            case 11:
                cardBack11.setVisible(true);
                break;
            case 12:
                cardBack12.setVisible(true);
                break;
            default: break;
        }
    }

    private void turnBackCard(Integer cardNumber){
        switch (whereIsTheCard.get(cardNumber)) {
            case 1 :
                cardBack1.setVisible(false);
                break;
            case 2:
                cardBack2.setVisible(false);
                break;
            case 3:
                cardBack3.setVisible(false);
                break;
            case 4 :
                cardBack4.setVisible(false);
                break;
            case 5 :
                cardBack5.setVisible(false);
                break;
            case 6:
                cardBack6.setVisible(false);
                break;
            case 7 :
                cardBack7.setVisible(false);
                break;
            case 8:
                cardBack8.setVisible(false);
                break;
            case 9:
                cardBack9.setVisible(false);
                break;
            case 10:
                cardBack10.setVisible(false);
                break;
            case 11:
                cardBack11.setVisible(false);
                break;
            case 12:
                cardBack12.setVisible(false);
                break;
            default: break;
        }
    }

    public class CardThreads extends Thread{

        private Integer s1 = selectedCard;
        private Integer s2 = selectedCard2;
        public void run () {
            try {
                Thread.sleep(500);
                turnBackCard(s1);
                turnBackCard(s2);
            } catch (InterruptedException ignored) {}
        }
    }

    private void clicking(Integer card) {
        if (!wasActionOnCard){
            genereateCardPositions();
            wasActionOnCard = true;
        }
        if (endtime == 0)
            return;
        if (selectedCard == 0) {
            turnCard(card);
            selectedCard = card;
        }else {
            turnCard(card);
            selectedCard2 = card;
            if (!serchForPair()){
                Thread waitingCards = new CardThreads();
                waitingCards.start();
            }
            selectedCard2 = 0;
            selectedCard = 0;
            if (coutor == 6){
                done = true;
                endOfGame();
            }
        }
    }

    public void clickedOnCard1(){
        Integer thisCard = 1;
        clicking(thisCard);
    }
    public void clickedOnCard2() {
        Integer thisCard = 2;
        clicking(thisCard);
    }
    public void clickedOnCard3() {
        Integer thisCard = 3;
        clicking(thisCard);
    }
    public void clickedOnCard4() {
        Integer thisCard = 4;
        clicking(thisCard);
    }
    public void clickedOnCard5() {
        Integer thisCard = 5;
        clicking(thisCard);
    }
    public void clickedOnCard6() {
        Integer thisCard = 6;
        clicking(thisCard);
    }
    public void clickedOnCard7() {
        Integer thisCard = 7;
        clicking(thisCard);
    }
    public void clickedOnCard8() {
        Integer thisCard = 8;
        clicking(thisCard);
    }
    public void clickedOnCard9() {
        Integer thisCard = 9;
        clicking(thisCard);
    }
    public void clickedOnCard10() {
        Integer thisCard = 10;
        clicking(thisCard);
    }
    public void clickedOnCard11() {
        Integer thisCard = 11;
        clicking(thisCard);
    }
    public void clickedOnCard12() {
        Integer thisCard = 12;
        clicking(thisCard);
    }

    public class runTheTimer extends Thread{

        public void run(){
            while(timer.getLength()>0 && !done){
                timer.setLength(timer.getLength()-6);
                try {
                    Thread.sleep(1000);
                }
                catch (InterruptedException ignored){}
            }
            endtime = 0;
            endOfGame();
        }
    }

    private boolean serchForPair(){

        for (Pair<Integer, Integer> position : positions) {
            if (position.getKey().equals(selectedCard) && position.getValue().equals(selectedCard2)) {
                coutor++;
                return true;
            }
            if (position.getValue().equals(selectedCard) && position.getKey().equals(selectedCard2)) {
                coutor++;
                return true;
            }
        }
        return false;
    }

    private void endOfGame(){
        Path path = Paths.get("src/sample/output.txt");

        Integer currentScore = (int)timer.getLength()/6;

        try (BufferedWriter writer = Files.newBufferedWriter(path))
        {
            writer.write(""+currentScore);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList <Integer> scores = new ArrayList<Integer>();

        String fileName = "src/sample/scores.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                Integer number = Integer.parseInt(line);
                scores.add(number);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Integer max = Collections.max(scores);

        Path path2 = Paths.get("src/sample/scores.txt");
        Charset charset = StandardCharsets.UTF_8;
        List<String> list = Collections.singletonList(""+currentScore);
        try {
            Files.write(path2, list, charset, new StandardOpenOption[]{StandardOpenOption.APPEND});
        } catch (IOException e) {
            e.printStackTrace();
        }

        timer.setVisible(false);

        if (currentScore > max){
            newRecord = true;
            timer.setVisible(false);
            end.setVisible(true);
        }else {
            newRecord = false;
            timer.setVisible(false);
            end.setVisible(true);
        }
    }

    @FXML
    public void endButtonPressed(ActionEvent event) throws IOException {
        if (newRecord){
            Parent game = FXMLLoader.load(getClass().getResource("EndScreenWithRecord.fxml")); //betolt
            Scene gameScene = new Scene(game, 400, 550);
            Stage gameStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            gameStage.setScene(gameScene);
            gameStage.show();
        }else{
            Parent game = FXMLLoader.load(getClass().getResource("EndScreen.fxml")); //betolt
            Scene gameScene = new Scene(game, 400, 550);
            Stage gameStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            gameStage.setScene(gameScene);
            gameStage.show();
        }
    }
}
