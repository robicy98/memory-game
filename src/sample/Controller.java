package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import java.io.IOException;


public class Controller {

    @FXML
    Button Start;

    @FXML
    Button Rekordok;

    @FXML
    Button Kilep;

    @FXML
    public void StartButton(ActionEvent event) throws IOException {
        Parent game = FXMLLoader.load(getClass().getResource("Game.fxml")); //betolt
        Scene gameScene = new Scene(game, 400, 550);
        Stage gameStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        gameStage.setScene(gameScene);
        gameStage.show();
    }

    @FXML
    public void RekordButton(ActionEvent event)throws IOException {
        Parent rekord = FXMLLoader.load(getClass().getResource("Rekord.fxml")); //betolt
        Scene gameScene = new Scene(rekord,400,550);
        Stage gameStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        gameStage.setScene(gameScene);
        gameStage.show();
    }

    @FXML
    public void ExitButton(ActionEvent event){
        Stage gameStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        gameStage.close();
    }
}


