package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;

public class Rekord  implements Initializable {

    @FXML
    TextField one;

    @FXML
    TextField two;

    @FXML
    TextField three;

    @FXML
    TextField four;

    @FXML
    TextField five;

    @FXML
    Button back;

    @FXML
    public void backButtonClicked(ActionEvent event) throws IOException {
        Parent main = FXMLLoader.load(getClass().getResource("sample.fxml")); //betolt
        Scene mainScene = new Scene(main, 400, 550);
        Stage mainStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        mainStage.setScene(mainScene);
        mainStage.show();
    }

    @FXML
    public void setScores(){
        ArrayList<Integer> scores = new ArrayList<Integer>();

        String fileName = "src/sample/scores.txt";

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                Integer number = Integer.parseInt(line);
                scores.add(number);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.sort(scores);
        scores.remove(0);
        Collections.sort(scores, Collections.reverseOrder());

        for (int i = 0; i < scores.size(); i++) {
            Integer num = scores.get(i);
            if (i == 0){
                one.setText("1. " + num);
            }
            if (i == 1){
                two.setText("2. " + num);
            }
            if (i == 2){
                three.setText("3. " + num);
            }
            if (i == 3){
                four.setText("4. " + num);
            }
            if (i == 4){
                five.setText("5. " + num);
            }if (i > 4){
                break;
            }
        }

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setScores();
    }
}
